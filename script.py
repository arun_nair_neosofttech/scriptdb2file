import os
import pymysql


# print(os.getcwd())
conn = pymysql.connect(
    host="localhost",
    user="root",
    password="root",
    db="arun_db"
)

with conn:
    cur = conn.cursor()
    cur.execute("SELECT * FROM biostats")
    desc = cur.description
    rows = cur.fetchall()
    with open("scriptdb2file.txt", "w") as textfile:
        for row in rows:
            line = ""
            for i in range(5):
                line += desc[i][0]+":"+str(row[i])+"\t"
            textfile.write(line+"\n")
